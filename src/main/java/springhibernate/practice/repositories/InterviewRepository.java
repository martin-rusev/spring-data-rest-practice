package springhibernate.practice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import springhibernate.practice.entities.Interview;

@RepositoryRestResource(path = "members")
public interface InterviewRepository extends JpaRepository<Interview,Integer> {
}
